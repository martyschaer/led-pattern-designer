var numLeds = 60 // 12/16/24/60
var degPerLed = 360 / numLeds
var alpha = 2 * Math.PI / numLeds
const circle = 2 * Math.PI
const ledSize = 15
var radius = numLeds * ledSize / 2
const borderSize = 3

var colors = new Array(numLeds).fill('#000000')


const canvas = document.getElementById('canvas')
canvas.height = canvas.offsetHeight
canvas.width = canvas.offsetWidth

canvas.addEventListener('mousedown', function (e) {
    onCanvasClick(canvas, e)
})

const ctx = canvas.getContext('2d')

draw(ctx)

function roundToNearest(n, roundTo) {
    numToRoundTo = 1 / (roundTo);

    return Math.round(n * numToRoundTo) / numToRoundTo
}

function calcOffset(index) {
    return index * alpha
}

function draw(ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.fillStyle = '#222222'
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    for (var i = 0; i < numLeds; i++) {
        const x = Math.cos(calcOffset(i)) * radius + canvas.width / 2
        const y = Math.sin(calcOffset(i)) * radius + canvas.height / 2
        ctx.fillStyle = '#000000'
        drawCircle(ctx, x, y, ledSize + borderSize)
        ctx.fillStyle = colors[i]
        drawCircle(ctx, x, y, ledSize)
    }
}

function drawCircle(ctx, x, y, radius) {
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI)
    ctx.fill();
}

const colorInput = document.getElementById('colorpicker')

function getInputColor() {
    return colorInput.value
}

function onCanvasClick(canvas, event) {
    const led = getCursorPosition(canvas, event)
    const color = getInputColor()
    colors[led] = color
    draw(ctx);
}

function getCursorPosition(canvas, event) {
    const rect = canvas.getBoundingClientRect()
    const x = event.clientX - rect.left - canvas.width / 2
    const y = event.clientY - rect.top - canvas.height / 2
    const theta = roundToNearest((Math.PI * 2 - Math.atan2(x, y) + Math.PI / 2) % (Math.PI * 2), alpha)
    const thetaDeg = theta * (180 / Math.PI)
    const ledIdx = Math.round((thetaDeg % 360) / degPerLed)
    return ledIdx
}

document.getElementById('button_download').onclick = function() {
    const mapped = colors.map(function(c) {
        if(c === '#ffffff') {
            return 0xff000000
        }
        return parseInt('0x00' + c[3] + c[4] + c[1] + c[2] + c[5] + c[6], 16)
    })

    download(JSON.stringify(mapped).replace('[', '{').replace(']', '}'))
}

const led_count_selector = document.getElementById('select_led_count')

led_count_selector.onchange = function() {
    const selected = led_count_selector.value
    numLeds = selected
    degPerLed = 360 / numLeds
    alpha = 2 * Math.PI / numLeds
    console.log(numLeds)
    colors = new Array(numLeds)

    for(var i = 0; i < numLeds; i++) {
        colors[i] = '#000000'
    }

    console.log(colors)
    radius = numLeds * ledSize / 2
    draw(ctx)
}

function download(content) {
    var a = document.createElement("a");
    var file = new Blob([content], {type: 'text/plain'});
    a.href = URL.createObjectURL(file);
    a.download = 'data.txt';
    a.click();
}